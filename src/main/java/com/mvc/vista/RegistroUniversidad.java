package com.mvc.vista;

import javax.swing.JOptionPane;

import com.mvc.controlador.UniversidadController;

public class RegistroUniversidad {
    // atributo
    private UniversidadController universidadController;

    // constructor
    public RegistroUniversidad() {
        this.solicitarDatos();
        this.consultarUniversidades();
    }

    public void solicitarDatos() {

        int cant_reg = Integer.parseInt(JOptionPane.showInputDialog(null, "¿Cuantas universidades desea registrar?"));
        this.universidadController = new UniversidadController(cant_reg);
        // iterar la cantidad de registros
        for (int i = 0; i < cant_reg; i++) {
            String nombre = JOptionPane.showInputDialog(null,
                    "Por favor ingrese el nombre de la universidad_" + (i + 1));
            String direccion = JOptionPane.showInputDialog(null,
                    "Por favor ingrese la dirección de la universidad " + nombre);
            String nit = JOptionPane.showInputDialog(null, "Por favor ingrese el nit de la universidad " + nombre);
            // crear una universidad
            this.universidadController.crear_universidad(nombre, direccion, nit, i);
            JOptionPane.showMessageDialog(null, "Universidad registrada con éxito");
        }
    }

    public void consultarUniversidades() {
        String info = "Por favor seleccione la universidad a consultar:\n\n";
        // Recorrer el arreglo
        for (int i = 0; i < this.universidadController.getUniversidades().length; i++) {
            info += (i + 1) + " -> " + this.universidadController.getUniversidad(i).getNombre() + "\n";
        }
        info += "0 -> Salir\n\n";
        int univ;
        do {
            // Mostrar información y capturar la opción digitada por el usuario
            univ = Integer.parseInt(JOptionPane.showInputDialog(null, info));
            if (univ != 0) {
                // Mostrar info de la universidad seleccionada
                JOptionPane.showMessageDialog(null, this.universidadController.getUniversidad((univ - 1)));
            }
        } while (univ != 0);
    }

}

/*****************************
 * 1 -> Univ 1 2 -> univ 2 ... 0 -> Salir
 ****************************/
