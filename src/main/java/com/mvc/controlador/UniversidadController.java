package com.mvc.controlador;

import com.mvc.modelo.Universidad;

public class UniversidadController {

    private Universidad[] universidades;

    public UniversidadController(int cant_universidades) {
        this.universidades = new Universidad[cant_universidades];
    }

    public void crear_universidad(String nombre, String direccion, String nit, int pos) {
        Universidad objUniversidad = new Universidad(nombre, direccion, nit);
        this.universidades[pos] = objUniversidad;
        
    }

    public Universidad[] getUniversidades(){
        return this.universidades;
    }

    public Universidad getUniversidad(int pos){
        return this.universidades[pos];
    }
}
