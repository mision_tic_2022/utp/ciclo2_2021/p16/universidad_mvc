package com.mvc.modelo;

public class Universidad {
    /*************
     * Atributos
     *************/
    private String nombre;
    private String direccion;
    private String nit;

    /**************************
     * 
     * Métodos Constructores
     * 
     **************************/

    public Universidad(String nombre, String direccion, String nit){
        this.nombre = nombre;
        this.direccion = direccion;
        this.nit = nit;
    }

    public Universidad(String nombre, String direccion){
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public Universidad(){
        System.out.println("Universidad creada!");
    }

    


    @Override
    public String toString() {
        String univStr = "\n--------"+nombre+"---------\n";
        univStr += "Dirección: "+direccion+"\n";
        univStr += "Nit: "+nit+"\n";
        return univStr;
    }

    /******************
     * Consultores
     ****************/

    public String getNombre() {
        return nombre;
    }

    public String getNit() {
        return nit;
    }

    public String getDireccion() {
        return direccion;
    }

    /******************
     * Modificadores
     ****************/

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public void crear_facultad(){
        
    }
    


}
